from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, ItemCreateForm

# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists
    }
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list
    }
    return render(request, "todos/todo_list_detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = ItemCreateForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemCreateForm()

    context = {
        "form": form,
    }
    return render(request, "todos/item_create.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemCreateForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = ItemCreateForm(instance=todo_item)
    context = {
        "form": form,
    }
    return render(request, "todos/item_update.html", context)


def edit_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "TodoList_object": todo_list,
        "TodoList_form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def delete_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/todo_list_delete.html")
